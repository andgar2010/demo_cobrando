// import Vue from 'vue';
import axios from 'axios';

export default async ({ Vue }) => {
  let baseURL = null;

  if (process.env.NODE_ENV === 'development') {
    baseURL = 'https://api_cobrando.tech-andgar.me/';
  } else if (process.env.NODE_ENV === 'QA') {
    baseURL = 'QA URL';
  } else {
    baseURL = 'https://api_cobrando.tech-andgar.me/';
  }

  Vue.prototype.$axios = axios.create({ baseURL });
};
