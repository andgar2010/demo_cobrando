import express from 'express';
const router: express.Router = express.Router();

import { index, uploadCSV } from '../controllers/index.controller';
import multer from '../libs/multer';

router
  .route('/')
  .get(index)
  .post(multer.single('file'), uploadCSV);

export default router;
