import { Router } from 'express';
const router: Router = Router();

import { tokenValidation } from '../libs/jwt';
import { signUp, signIn, profile } from '../controllers/auth.controllers';

router
  .post('/signup', signUp)
  .post('/signin', signIn)
  .get('/profile', tokenValidation, profile);

export default router;
