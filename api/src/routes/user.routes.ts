import { Router } from 'express';
const router: Router = Router();

import { uploadFileCSV } from '../controllers/user.controllers';
import multer from '../libs/multer';

router.route('/uploadFileCSV').post(multer.single('file'), uploadFileCSV);

export default router;
