import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';
import path from 'path';
import { format } from 'date-fns';

/*
import { format, utcToZonedTime } from 'date-fns-tz'

const storage = multer.diskStorage({
  destination: 'uploads',
  filename: (req, file, cb) => {
    const date = new Date()
    const timeZone = 'America/Bogota'
    const zonedDate = utcToZonedTime(date, timeZone)
    const dateNameFile = `${format(zonedDate, 'yyyy_MM_dd_HH_mm')}__${uuidv4()}`
    cb(null, dateNameFile + path.extname(file.originalname))
  }
})
*/

const storage = multer.diskStorage({
  destination: 'uploads',
  filename: (req, file, cb) => {
    const dateNameFile = `${format(new Date(), 'yyyy_MM_dd_HH_mm')}__${uuidv4()}`;
    cb(null, dateNameFile + path.extname(file.originalname));
  },
});

export default multer({ storage });
