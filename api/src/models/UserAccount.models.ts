import { Schema, model, Document } from 'mongoose';
import bcryptjs from 'bcryptjs';

export interface TUserAccount extends Document {
  username: string;
  email: string;
  password: string;
  encryptPassword(password: string): Promise<string>;
  validatePassword(password: string): Promise<boolean>;
}

const userAccountSchema = new Schema({
  username: {
    type: String,
    required: true,
    min: 4,
    lowercase: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  password: {
    type: String,
    required: true,
  },
  any: Schema.Types.Mixed,
});

userAccountSchema.methods.encryptPassword = async (password: string): Promise<string> => {
  const salt = await bcryptjs.genSalt(10);
  return bcryptjs.hash(password, salt);
};

userAccountSchema.methods.validatePassword = async function(password: string): Promise<boolean> {
  return await bcryptjs.compare(password, this.password);
};

export default model<TUserAccount>('User_Account', userAccountSchema);
