import { Schema, model, Document } from 'mongoose';

const userSchema = new Schema({}, { strict: false });
// const userSchema = new Schema({
//   any: Schema.Types.Mixed,
// });

export default model<Document>('User', userSchema);
