import mongoose from 'mongoose';

export async function connectionDB() {
  try {
    await mongoose.connect('mongodb://localhost/test', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    console.log('Database is connected');
  } catch (error) {
    console.error(error);
  }
}
