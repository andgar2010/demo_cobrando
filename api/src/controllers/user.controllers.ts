import { unlinkSync, existsSync, readFileSync } from 'fs';
import { Request, Response } from 'express';
import { parse } from 'papaparse';

import UserModel from '../models/User.models';

import jwt from 'jsonwebtoken';

export function indexAuth(req: Request, res: Response): Response {
  console.log('index auth');
  return res.send('Get Auth user');
}

function checkTypeMime(req: Request, typeMime: string[]) {
  return typeMime.includes(req.file.mimetype);
}

function checkFileUpload(req: Request) {
  try {
    return existsSync(req.file.path);
  } catch {
    return false;
  }
}

function deleteFile(req: Request) {
  try {
    return unlinkSync(req.file.path);
  } catch (err) {
    console.error(err);
  }
}

function converterCSVtoJSON(req: Request) {
  const file = readFileSync(req.file.path, 'utf8');
  const config = {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    complete: function(_result: any) {
      // console.dir(result.data);
      // console.log('Parsing complete:', result, file);
    },
    // delimiter: '', // auto-detect
    // newline: '', // auto-detect
    // quoteChar: '"',
    // escapeChar: '"',
    header: true,
    // transformHeader: undefined,
    dynamicTyping: true,
    // preview: 0,
    // encoding: '',
    // worker: false,
    // comments: false,
    // step: undefined,
    // error: undefined,
    // download: false,
    // downloadRequestHeaders: undefined,
    skipEmptyLines: true,
    // chunk: undefined,
    // fastMode: undefined,
    // beforeFirstChunk: undefined,
    // withCredentials: undefined,
    // transform: undefined,
    // delimitersToGuess: [',', '\t', '|', ';', Papa.RECORD_SEP, Papa.UNIT_SEP],
  };
  return parse(file, config);
}

function updateDataSchemaDynamic(req: Request) {
  const entries: string[] = Object.keys(req.body);
  const updates: any = {};

  // constructing dynamic query
  for (let i = 0; i < entries.length; i++) {
    updates[entries[i]] = Object.values(req.body)[i];
  }

  try {
    const resultDB = UserModel.update({ username: req.params.user }, { $set: updates });
    console.log(resultDB);
    // res.send({ msg: 'update success' });
  } catch (error) {
    console.error(error);
  }
}

export async function uploadFileCSV(req: Request, res: Response): Promise<Response> {
  // console.log('Recvicaed file');
  // const obj = JSON.parse(JSON.stringify(req.body));
  // console.log(obj);
  const typeMime = ['text/csv', 'text/txt', 'application/vnd.ms-excel'];
  if (!checkFileUpload(req)) {
    return res.status(400).json({ message: 'No correct upload, please again' });
  }
  if (!checkTypeMime(req, typeMime)) {
    deleteFile(req);
    return res.status(400).json({ message: 'This file is no admit format' });
  }
  const usersJson = converterCSVtoJSON(req).data;
  const userSaved = await UserModel.insertMany(usersJson);
  return res.json(userSaved);

  // let token: string | undefined;
  // let userSaved;
  // try {
  //   // SAVING USER ON DB
  //   const user: TUser = new User({
  //     username: req.body.username,
  //     email: req.body.email,
  //     password: req.body.password,
  //   });
  //   user.password = await user.encryptPassword(user.password);
  //   userSaved = await user.save();
  //   console.log(userSaved);

  //   // GENERATE TOKEN
  //   token = jwt.sign({ _id: userSaved.id }, process.env.TOKEN_SECRET || 'temptoken');
  //   return res.header('auth-token', token).json(userSaved);
  // } catch (error) {
  //   console.error(error);
  //   return res.json(error?.errmsg || error);
  // }
}

// export async function signIn(req: Request, res: Response): Promise<Response> {
//   try {
//     const user = await User.findOne({ email: req.body.email });
//     if (!user) return res.status(400).json('Email or password is wrong');

//     const correctPassword: boolean = await user.validatePassword(req.body.password);
//     if (!correctPassword) return res.status(400).json('Invalid Password');

//     const token = jwt.sign({ _id: user.id }, process.env.TOKEN_SECRET || 'temptoken');
//     return res.header('auth-token', token).json(token);
//   } catch (error) {
//     return res.status(400).json(error?.errmsg ?? error);
//   }
// }

// export const profile = async (req: Request, res: Response) => {
//   const user = await User.findById(req.userId, { password: 0 });
//   if (!user) {
//     return res.status(404).json('No User found');
//   }
//   res.json(user);
// };

// export function authenticatorUser(req: Request, res: Response): Response {
//   console.log('Recvibed file');
//   // const obj = JSON.parse(JSON.stringify(req.body));
//   // console.log(obj);

//   return res.json({ text: 'Post authenticator User' });
// }
