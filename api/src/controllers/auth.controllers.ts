import { Request, Response } from 'express';
import User, { TUserAccount } from '../models/UserAccount.models';

import jwt from 'jsonwebtoken';

export function indexAuth(req: Request, res: Response): Response {
  console.log('index auth');
  return res.send('Get Auth user');
}

export async function signUp(req: Request, res: Response): Promise<Response> {
  let token: string | undefined;
  let userSaved;
  try {
    // SAVING USER ON DB
    const user: TUserAccount = new User({
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
    });
    user.password = await user.encryptPassword(user.password);
    userSaved = await user.save();
    console.log(userSaved);

    // GENERATE TOKEN
    token = jwt.sign({ _id: userSaved.id }, process.env.TOKEN_SECRET || 'temptoken');
    return res.header('auth-token', token).json(userSaved);
  } catch (error) {
    console.error(error);
    return res.json(error?.errmsg || error);
  }
}

export async function signIn(req: Request, res: Response): Promise<Response> {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(400).json('Email or password is wrong');

    const correctPassword: boolean = await user.validatePassword(req.body.password);
    if (!correctPassword) return res.status(400).json('Invalid Password');

    const token = jwt.sign({ _id: user.id }, process.env.TOKEN_SECRET || 'temptoken');
    return res.header('auth-token', token).json(token);
  } catch (error) {
    return res.status(400).json(error?.errmsg ?? error);
  }
}

export const profile = async (req: Request, res: Response) => {
  const user = await User.findById(req.userId, { password: 0 });
  if (!user) {
    return res.status(404).json('No User found');
  }
  res.json(user);
};

export function authenticatorUser(req: Request, res: Response): Response {
  console.log('Recvibed file');
  // const obj = JSON.parse(JSON.stringify(req.body));
  // console.log(obj);

  return res.json({ text: 'Post authenticator User' });
}
