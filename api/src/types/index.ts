/* eslint-disable @typescript-eslint/no-namespace */
// interface Request {
//   userId: string;
// }

// declare namespace Express {
//   export interface Request extends Request{
//     userId: string;
//   }
// }

// import { Request } from 'express';
// interface TUserRequest extends Request {
//   userId: string;
//   user: {
//     name: string;
//     surname: string;
//     age: number;
//   };
// }

declare namespace Express {
  interface Request {
    customProperties: string[];
    userId: string;
  }
}
