import { PORT } from './configuration';

import express, { Request, Response, Application } from 'express';
// import express = require('express');
// import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';

import indexV1Routes from './routes/indexV1.routes';
import authRoutes from './routes/auth.routes';
import userRoutes from './routes/user.routes';
import path from 'path';

function loggerMiddleware(request: Request, response: Response, next: () => void) {
  console.log(`Log: ${request.method} ${request.path}`);
  // console.log(request);
  next();
}

// const app: express.Application = express();
const app: Application = express()
  .set('port', PORT) // SETTINGS
  .use(cors()) // CORS
  .use(morgan('dev')) // MIDDLEWARES
  .use(loggerMiddleware) // MIDDLEWARES
  .use(express.json()) // for parsing application/json
  // .use(bodyParser.json());
  .use(express.urlencoded({ extended: true })) // for parsing application/xwww-form-urlencoded
  // .use(bodyParser.urlencoded({ extended: false }));
  .use('/uploads', express.static(path.resolve('uploads'))) // SET FOLDER UPLOAD
  //                          // for parsing multipart/form-data
  .use('/v1', indexV1Routes) // ROUTES
  .use('/auth', authRoutes) // ROUTES
  .use('/user', userRoutes) // ROUTES
  .get('/api/v1/test', (_, res) => {
    res.json({ ok: true });
  });

export const getApp = async () => {
  const app: Application = express()
    .set('port', PORT) // SETTINGS
    .use(cors()) // CORS
    .use(morgan('dev')) // MIDDLEWARES
    .use(loggerMiddleware) // MIDDLEWARES
    .use(express.json()) // for parsing application/json
    // .use(bodyParser.json());
    .use(express.urlencoded({ extended: true })) // for parsing application/xwww-form-urlencoded
    // .use(bodyParser.urlencoded({ extended: false }));
    .use('/uploads', express.static(path.resolve('uploads'))) // SET FOLDER UPLOAD
    //                          // for parsing multipart/form-data
    .use('/v1', indexV1Routes) // ROUTES
    .use('/auth', authRoutes) // ROUTES
    .use('/user', userRoutes) // ROUTES
    .get('/api/v1/test', (_, res) => {
      res.json({ ok: true });
    });
  return app;
};

export default app;
