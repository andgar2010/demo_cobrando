import('dotenv').then(dotenv => {
  dotenv.config();
});

import app from './app';
// import { connectionDB } from './database';

const ip = require('os').networkInterfaces().eth0[0].address;

const startServer = async () => {
  try {
    await app.listen(app.get('port'));
    console.log(`server started at http://${ip}:${app.get('port')}`);

    const { connectionDB } = await import('./database');
    connectionDB();
  } catch (error) {
    console.error(error);
  }
};

startServer();
